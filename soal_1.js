function return_biodata(){
  let biodata = {
    "name" : "Agustin Parmaini",
    "age" : 24,
    "address" : "Jl. Kapten A. Rivai Lorong Karya No 248 RT 002 RW 002 Palembang",
    "hobbies" : ['Travelling','cooking'];
    "is_married" : false;
    "list_school" : [
      {
        "name" : "SMA Muhammadiyah 2 Palembang",
        "year_in" : "2010",
        "year_out" : "2013",
        "major" : "IPA"
      },
      {
        "name" : "STMIK GI MDP Palembang",
        "year_in" : "2013",
        "year_out" : "2017",
        "major" : "Sistem Informasi"
      }
    ],
    "skills" : [
      {
        "skill_name" : "PHP",
        "level" : "Beginner"
      },
      {
        "skill_name" : "Web Design",
        "level" : "Advanced"
      }
    ],
    "interest_in_coding" : true
  }
  return biodata;
}


//console.log(return_biodata());